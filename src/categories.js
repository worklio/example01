define(['collection/categories'], function (collectionCategories) {    
    return new collectionCategories([
        {"id": 1,   "name": "Beverages",    "examples":["coffee", "tea", "juice", "soda"]},
        {"id": 2,   "name": "Bakery",       "examples":["bread", "sandwich loaves", "dinner rolls", "tortillas", "bagels"]},
        {"id": 3,   "name": "Dairy",        "examples":["cheeses", "eggs", "milk", "yogurt", "butter"]},
        {"id": 4,   "name": "Meat",         "examples":["lunch meat", "poultry", "beef", "pork"]},
        {"id": 5,   "name": "Produce",      "examples":["fruits", "vegetables"]},
        {"id": 6,   "name": "Other"}
    ]);
});