define(function(require, exports, module) {
    window.Log = module.config()===true ? window.console.log.bind(window.console) : function(){};
    window.Warn = module.config()===true ? window.console.warn.bind(window.console) : function(){};
});