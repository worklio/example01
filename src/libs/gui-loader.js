define(['jquery'],function($){

    function Loader(elm){
        this.elm= elm ? $(elm) : $(document.body);
        this.cont=$('<div class="gui-loader"><div class="gui-loader-overlay"></div><div class="loading-wrap"><div class="loading"><span class="loading-dot"></span><span class="loading-dot"></span><span class="loading-dot"></span></div></div></div>');
        this.elm.addClass('gui-loading');
        this.elm.prepend(this.cont);
    }

    Loader.prototype.remove=function(){
        this.cont.remove();
        this.elm.removeClass('gui-loading');
    }

    return Loader;
});