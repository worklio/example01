define(['jquery', 'libs/gui-loader'],function($, guiLoader){
    function Loader(){
        this.count=0;
    }

    Loader.prototype.selector=function(){
        var def = 'body';
        var cont=$('.app>.container');


        return cont.length ? d : def;
    }

    Loader.prototype.showNow=function(selector){
        Log('show now');
        clearTimeout(this.hideTimer);
        delete this.hideTimer;
        clearTimeout(this.timer);
        delete this.timer;
        this.count++; 
        if(this.loader==undefined)
            this.loader=new guiLoader(selector || this.selector());
    }

    Loader.prototype.show=function(){

        this.count++; 
        Log('+'+this.count, this.hideTimer != undefined ? 'hide on progress':'');

        clearTimeout(this.hideTimer);
        delete this.hideTimer;

        if(this.loader==undefined && this.timer == undefined){
            this.timer=setTimeout(function(){
                Log('show');
                this.loader=new guiLoader(this.selector());
            }.bind(this),200);
        }
    }
    
    Loader.prototype.hide=function(){

        if(this.count>0) this.count--;
        Log('-'+this.count);
        if(this.count==0 && (this.loader != undefined || this.timer != undefined)){
            clearTimeout(this.timer);
            delete this.timer;
            if(this.loader != undefined && this.hideTimer == undefined){
                this.hideTimer=setTimeout(function(){
                    Log('hide');
                    this.loader.remove();
                    delete this.loader;
                    delete this.hideTimer;
                }.bind(this),200);
                Log('set hide',this.hideTimer);
            }
        }
    }

    Loader.prototype.hideAll=function(){
        while(this.count) this.hide();
    }

    return Loader;
});