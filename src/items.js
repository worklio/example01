define(['collection/items'], function (collectionItems) {    
    return new collectionItems([
        {"label":"1 milk", "category": 3},
        {"label":"half loaf of bread", "category": 2},
        {"label":"10 bagels", "category": 2},
        {"label":"1 kg of coffee beans", "category": 1},
        {"label":"2 tortillas", "category": 2},
        {"label":"1 kg of apples", "category": 2}
    ]);
});