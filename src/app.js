define([
    'ko', 'views', 'loader', 'model/menuitem', 'categories', 'items'
], function(
    ko, Views, Loader, MenuItem, Categories, Items
) {
    
    function App(){
        Loader.showNow('body');

        this.callbacks = [];
        this._loaded=false;
        
        this._loaded=true;
        
        this.view=Views.active;
        this.page=ko.pureComputed(function(){
            return this.view() && this.view().page
        },this);


        this.menuitems = ko.observableArray(ko.utils.arrayMap([
            {view:"list", label: "List"},
            {view:"categories", label: "Categories"}
        ],function(menuitem){return new MenuItem(menuitem)}));


        Loader.hide();

        while (this.callbacks.length) this.callbacks.shift().call();

                
        window.__page_loaded=true;

    }

    App.prototype.onready=function(callback){
        Log('Test if app is ready');
        
        if(this._loaded)
            callback.call()
        else
            this.callbacks.push(callback);
    }



    var app = new App();
    app.onready(function(){
        Views.push('list');
    });

    return app;

});