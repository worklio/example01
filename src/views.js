define([
    'ko','loader'
], function (
    ko,Loader
) {

    
    function modelViews() {
        this.views = [];

        this.active = ko.observable();

        this.baseUrl = ko.observable('');
        this.module = ko.observable();
        
        this.module.subscribe(function(module){
            var self=this;
            Loader.show();
            Log('Get module "'+module+'"');
            require(
                ['module/'+module],
                function(newModule){ 
                    Log('Module "'+module+'" was downloaded');
                    new newModule(self.baseUrl());
                    Loader.hide();
                },
                function(err){
                    Warn(err);
                    self.pushError({status:404});
                    Loader.hide();
                }
            )



        },this);

    }

    modelViews.prototype.switchModule = function(module,baseUrl){
        this.baseUrl(baseUrl);
        this.module(module);
    }

    modelViews.prototype.push = function(name,param){
        var self=this;
        Loader.show();
        var index = name.indexOf('/'),
            module, url, logMsg;

        if(index != -1){
            module = name.substring(0,index);
            name = name.substr(index+1);
            url = 'module/'+module+'/view/'+name;
            logMsg = 'View "'+name+'" from module "'+module+'"';
        }
        else{
            url = 'view/'+name;
            logMsg = 'View "'+name+'"';
        }


        Log(logMsg+' is loading...');
        require(
            [url],
            function(newView){ 
                Log(logMsg+' loaded');
                self.views.push(new newView(param));
                Loader.hide();
            },
            function(err){
                Warn(err);
                self.pushError({status:404});
                Loader.hide();
            }
        )

    }

    modelViews.prototype.pushError = function(param){
        this.clear();
        var viewError = require("view/error");
        this.views.push(new viewError(param));
    }


    modelViews.prototype.clear = function(){
        while (this.views.length > 0 ){
            var viewToDestroy = this.views.pop();
            if(typeof viewToDestroy.destroy == 'function') viewToDestroy.destroy();
        }

    }



    Log('Create views');
    return new modelViews();

});