define([
    'ko', 'views'
], function(
    ko, Views
) {


    function MenuItem(data){
        this.label = ko.observable(data.label);
        this.view = ko.observable(data.view);

        this.isActive = ko.pureComputed(function(){
            return Views.active() && Views.active().name == this.view()
        },this);
    }

    MenuItem.prototype.open = function(){
        Views.push(this.view());
    }




    return MenuItem;

});