define([
    'ko'
], function(
    ko
) {


    function Category(data){
        this.id = ko.observable(data.id);
        this.name = ko.observable(data.name);
    }




    return Category;

});