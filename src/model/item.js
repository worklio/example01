define([
    'ko', 'categories'
], function(
    ko, Categories
) {


    function Item(data){
        this.id = ko.observable(data.id);
        this.label = ko.observable(data.label);
        this.category = ko.pureComputed(function(){
            return Categories.get(data.category);
        },this);

        this.categoryName = ko.pureComputed(function(){
            return this.category() && this.category().name();
        },this);


    }




    return Item;

});