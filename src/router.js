define(['crossroads', 'hasher',  'views'], function ( crossroads, hasher, views) {


    function Router(config) {
        this.params = ko.observable({});
        this.previousPath = ko.observable(null);

        this.mainRouter = crossroads.create();
        this.mainRouter.normalizeFn = crossroads.NORM_AS_OBJECT;
        //this.mainRouter.ignoreState = true;
        this.moduleRouter = crossroads.create();
        this.moduleRouter.normalizeFn = crossroads.NORM_AS_OBJECT;
        //this.moduleRouter.ignoreState = true;

        
        this.model = null;

        this.module = views.module;

        this.module.subscribe(function(){
            this.mainRouter.unpipe(this.moduleRouter);
        },this);
    }

    Router.prototype.start=function(config){
        Log('Router Starting');
        var self=this;
        ko.utils.arrayForEach(config.routes, function(route) {
            var url =  route.url == '' ?  '' : new RegExp( route.url + "[/]{0,1}.*");

           
            var r = self.mainRouter.addRoute(url, function() {
                Log('Match main route ' + route.url);
                views.switchModule(route.module, route.url);
            });
        });


        var r = self.mainRouter.addRoute(/.*/, function() {
            Log('Match no route');
            //views.pushError({status:404});
            location.replace('#');
        })

        activateCrossroads(this);
    }

    Router.prototype.startModule=function(module,baseUrl,config){
        Log('Module Router Starting');
        
        var self=this;

        self.moduleRouter.removeAllRoutes();
        self.moduleRouter.resetState();
 

        ko.utils.arrayForEach(config.routes, function(route) {
            var url =  baseUrl + '/' + route.url;
            
            var r = self.moduleRouter.addRoute(url, function(requestParams) {
                Log('Math module route ' + route.url);
                self.params(requestParams);
                views.push(module+'/'+route.view,self.model);
                self.model = null;
            });

            if(route.rules){
                Log(route.rules)
                r.rules = route.rules;
            }
        });

        var r = self.moduleRouter.addRoute(/.*/, function() {
            Log('Match no route in module');
            //views.pushError({status:404});
            location.replace('#');
        })

        this.moduleRouter.parse(hasher.getHash());
      
        this.mainRouter.pipe(this.moduleRouter);

    }



    Router.prototype.set = function(url,model){
        var actualHash = hasher.getHash();
        var newHash = url.replace(/^#/,'').replace(/^\//,'');
        
        if(actualHash!=newHash){
            Log(url,model);
            this.model = model;
            hasher.setHash(newHash);
        }
    }
    Router.prototype.back = function(){
        Log('back');
        if(this.previousPath()){
            history.back();
        }
        else
            this.set(views.baseUrl())
    }

    function activateCrossroads(router) {
        function parseHash(newHash, oldHash){ 
            router.previousPath( oldHash );
            router.mainRouter.parse(newHash);
        }


        hasher.initialized.add(parseHash);
        hasher.changed.add(parseHash);
        hasher.init();
    }





    Log('Created router');

    return new Router();

});