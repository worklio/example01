define([
    'ko','text!./page-list.html', 'categories',

    'component/item'
],
function(
    ko,Template, Categories
) {

    function ViewModel(model,element){
        this.items = model.data;
        this.categories = Categories.data;

        this.newItemLabel = ko.observable();
        this.newItemCategory = ko.observable(6);

        this.add = function(){
            if(this.newItemLabel()){
                model.add({
                    label:this.newItemLabel(),
                    category: this.newItemCategory()
                });

                this.newItemLabel('');
                this.newItemCategory(6);
            }
        }
    }


    


    function ViewModelFactory(view, componentInfo) {
        view.element=componentInfo.element;
        return new ViewModel(view.model,view.element);
    }

    return { viewModel: {createViewModel: ViewModelFactory}, template: Template };

});