define([
    'text!./item.html'
],
function(
    Template
) {


    function ViewModelFactory(model, componentInfo) {
        return model;
    }

    return { viewModel: {createViewModel: ViewModelFactory}, template: Template };

});