define([
    'text!./page-categories.html',

    'component/category'
],
function(
    Template
) {

    function ViewModel(model,element){
        this.categories = model.data;
    }
    


    function ViewModelFactory(view, componentInfo) {
        view.element=componentInfo.element;
        return new ViewModel(view.model,view.element);
    }

    return { viewModel: {createViewModel: ViewModelFactory}, template: Template };

});