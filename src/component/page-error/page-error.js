define([
    'text!./page-error.html'
],
function(
    Template
) {

    function ViewModelFactory(view, componentInfo) {
        view.element=componentInfo.element;
        Log(view.model);
        return {
            headline:view.model.headline,
            text:view.model.text
        };
    }

    return { viewModel: {createViewModel: ViewModelFactory}, template: Template };
});