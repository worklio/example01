require([
    'ko','jquery','log', 'config', 'view/error', 'app'
],

 

function(ko,$,Log, Config, viewError, App) {
    $(function(){
        ko.applyBindings(App, $('.app').get(0));
    });
});