define([
    'ko', 'model/item'
], function(
    ko, Item
) {


    function Items(data){
        Log('Create collection Items');

        this.data = ko.observableArray();

        if(data) this.set(data);

    }
    

    Items.prototype.get=function(id){
        return ko.utils.arrayFirst(this.data(),function(p){return p.id() == id});  
    }
    
    Items.prototype.set=function(data){
        this.data(ko.utils.arrayMap(data,function(data_item){return new Item(data_item)}))
    }

    Items.prototype.add=function(data){
        this.data.push(new Item(data));
    }


    return Items;

});