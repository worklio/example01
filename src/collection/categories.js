define([
    'ko', 'model/category'
], function(
    ko, Category
) {


    function Categories(data){
        Log('Create collection Categories');

        this.data = ko.observableArray();


        if(data) this.set(data);

    }
    

    Categories.prototype.get=function(id){
        return ko.utils.arrayFirst(this.data(),function(p){return p.id() == id});  
    }
    
    Categories.prototype.set=function(data){
        this.data(ko.utils.arrayMap(data,function(data_item){return new Category(data_item)}))
    }

    return Categories;

});