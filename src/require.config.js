/**
 * JS Object to configure paths for RequireJS.
 *
 */


var require = {
    paths: {
        "jquery":               "bower_modules/jquery/dist/jquery",
        "ko":                   "bower_modules/knockout/dist/knockout.debug",
        "log":                  "libs/log",
        "text":                 "bower_modules/requirejs-text/text",
        "moment":               "bower_modules/moment/min/moment-with-locales.min",
        "jqueryui":             "bower_modules/jquery-ui/ui"
    },
    urlArgs: 'ver=5'
};
