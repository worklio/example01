define([
    'app', 'categories',

    'component/page-categories'
], function(
    App, Categories
) {

    function View(){
        this.name = "categories";

        this.page='page-categories';
        this.model = Categories;
        this.show();
    
    }

    View.prototype.show=function(){
        App.view(this);
    }


    View.prototype.destroy=function(){
    }


    return View;

});