define([
    'app', 'ko',
    'component/page-error'
], function(
    App, ko
) {

    function View(data){
        Log(data);
        this.page = "page-error";

        if(data==undefined)data={};

        var headline,text;
        
        switch(+data.status){
            case 0:
                headline="Unable to connect to the Internet";
                text="We cannot display this page because your computer is not connected to the Internet.";
                break;
            case 403:
                headline="Access denied";
                text="You do not have enough rights to perform this operation. Please contact your system administrator for more information.";
                break;
            case 404:
                headline="This webpage is not available.";
                break;
            default:
                headline="Error occured";
                text="There is a temporary problem with this particular service. We are aware of the situation and we are trying to fix it as quickly as possible. Please try again or return in a few minutes. We apologize for the inconvenience.";
                break;
        }

        this.model = {
            headline: ko.observable(data.headline || headline),
            text: ko.observable(data.text || text)
        };
        
        this.show();
    }

    View.prototype.show=function(){
        App.view(this);
    }


    View.prototype.destroy=function(){
    }

    

    return View;

});