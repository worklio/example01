define([
    'app', 'items',

    'component/page-list'
], function(
    App, Items
) {

    function View(){
        this.name = "list";

        this.page='page-list';
        this.model = Items;
        this.show();
    
    }

    View.prototype.show=function(){
        App.view(this);
    }


    View.prototype.destroy=function(){
    }


    return View;

});